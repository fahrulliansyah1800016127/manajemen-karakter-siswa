import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-cari',
  templateUrl: './cari.page.html',
  styleUrls: ['./cari.page.scss'],
})
export class CariPage implements OnInit {

  kode: string;
  id: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.kode = this.actRoute.snapshot.paramMap.get("kode");
  }
 
 
  ngOnInit() {
    this.cariKode(this.kode);
  }

  async cariKode(kode: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Please wait..."
    });
    loader.present();

    this.firestore
      .doc("kelass/" + kode)
      .valueChanges()
      .subscribe(data => {
        this.kelas.nkelas = data["nkelas"];
        this.kelas.nguru = data["nguru"];
        this.kelas.kode = data["kode"];

        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(kelas: Kelas) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("kelass/" + this.id).update(kelas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("hasil");
    }
  }

  formValidation() {
    if (!this.kelas.nkelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.kelas.nguru) {
      // show toast message
      this.showToast("Enter Nama Guru");
      return false;
    }

    if (!this.kelas.kode) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
