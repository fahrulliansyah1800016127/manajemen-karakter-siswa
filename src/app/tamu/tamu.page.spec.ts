import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TamuPage } from './tamu.page';

describe('TamuPage', () => {
  let component: TamuPage;
  let fixture: ComponentFixture<TamuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TamuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
