import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';



import { TabsPage } from './tabs.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
    {
      path: 'utama-guru',
      children: [
        {
          path: '',
          loadChildren: '../utama-guru/utama-guru.module#UtamaGuruPageModule'
        }
      ]
    },
    {
      path: 'chat',
      children: [
        {
          path: '',
          loadChildren: '../chat/chat.module#ChatPageModule'
        }
      ]
    },
    {
      path: 'profile',
      children: [
        {
          path: '',
          loadChildren: '../profile/profile.module#ProfilePageModule'
        }
      ]
    },
    {
      path: 'setting',
      children: [
        {
          path: '',
          loadChildren: '../setting/setting.module#SettingPageModule'
        }
      ]
    },
    {
      path: 'laporan',
      children: [
        {
          path: '',
          loadChildren: '../laporan/laporan.module#LaporanPageModule'
        }
      ]
    },
    {
      path: 'kelas-guru',
      children: [
        {
          path: '',
          loadChildren: '../kelas-guru/kelas-guru.module#KelasGuruPageModule'
        }
      ]
    },
    {
      path: 'bimbingan-guru/:id',
      children: [
        {
          path: '',
          loadChildren: '../bimbingan-guru/bimbingan-guru.module#BimbinganGuruPageModule'
        }
      ]
    },
  ]
  },
  {
    path: '',
    redirectTo: 'tabs/utama-guru',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [TabsPage],
  exports: [RouterModule]
})
export class TabsPageModule {}
