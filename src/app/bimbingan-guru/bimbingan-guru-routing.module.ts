import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BimbinganGuruPage } from './bimbingan-guru.page';

const routes: Routes = [
  {
    path: '',
    component: BimbinganGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BimbinganGuruPageRoutingModule {}
