import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController, Platform, AlertController } from '@ionic/angular';
import { User } from '../models/user';

@Component({
  selector: 'app-bimbingan-guru',
  templateUrl: './bimbingan-guru.page.html',
  styleUrls: ['./bimbingan-guru.page.scss'],
})
export class BimbinganGuruPage implements OnInit {
  xipa = {} as User;
  id: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.id = this.actRoute.snapshot.paramMap.get("id");
  }
 
 
  ngOnInit() {
    this.getKelasById(this.id);
  }

  async getKelasById(id: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Please wait..."
    });
    loader.present();

    this.firestore
      .doc("xipa/" + id)
      .valueChanges()
      .subscribe(data => {
        this.xipa.namasiswa = data["namasiswa"];
        this.xipa.namakelas = data["namakelas"];
        this.xipa.karakter = data["karakter"];
        this.xipa.penanganan = data["penanganan"];

        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(xipa: User) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("xipa/" + this.id).update(xipa);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("x-ipa");
    }
  }

  formValidation() {
    if (!this.xipa.namasiswa) {
      // show toast message
      this.showToast("Enter Nama Siswa");
      return false;
    }

    if (!this.xipa.namakelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.xipa.karakter) {
      // show toast message
      this.showToast("Enter Karakter");
      return false;
    }

    if (!this.xipa.penanganan) {
      // show toast message
      this.showToast("Enter Penanganan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
