import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BimbinganGuruPage } from './bimbingan-guru.page';

describe('BimbinganGuruPage', () => {
  let component: BimbinganGuruPage;
  let fixture: ComponentFixture<BimbinganGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BimbinganGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BimbinganGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
