import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BimbinganGuruPageRoutingModule } from './bimbingan-guru-routing.module';

import { BimbinganGuruPage } from './bimbingan-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BimbinganGuruPageRoutingModule
  ],
  declarations: [BimbinganGuruPage]
})
export class BimbinganGuruPageModule {}
