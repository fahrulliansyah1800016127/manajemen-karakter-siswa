import { Component, OnInit } from '@angular/core';

import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-add-x-ipa',
  templateUrl: './add-x-ipa.page.html',
  styleUrls: ['./add-x-ipa.page.scss'],
})
export class AddXIpaPage implements OnInit {

  xipa ={} as User;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) {}
  
  ngOnInit() {}


  async createKelas(xipa: User) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("xipa").add(xipa);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("x-ipa");
    }
  }

  formValidation() {
    if (!this.xipa.namasiswa) {
      // show toast message
      this.showToast("Enter Nama Siswa");
      return false;
    }

    
    if (!this.xipa.namakelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.xipa.phone) {
      // show toast message
      this.showToast("Enter No Telp");
      return false;
    }


    if (!this.xipa.karakter) {
      // show toast message
      this.showToast("Enter Karakter");
      return false;
    }

    if (!this.xipa.penanganan) {
      // show toast message
      this.showToast("Enter Penanganan");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}
