import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddXIpaPageRoutingModule } from './add-x-ipa-routing.module';

import { AddXIpaPage } from './add-x-ipa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddXIpaPageRoutingModule
  ],
  declarations: [AddXIpaPage]
})
export class AddXIpaPageModule {}
