import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddXIpaPage } from './add-x-ipa.page';

const routes: Routes = [
  {
    path: '',
    component: AddXIpaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddXIpaPageRoutingModule {}
