import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddXIpaPage } from './add-x-ipa.page';

describe('AddXIpaPage', () => {
  let component: AddXIpaPage;
  let fixture: ComponentFixture<AddXIpaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddXIpaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddXIpaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
