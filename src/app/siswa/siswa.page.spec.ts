import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SiswaPage } from './siswa.page';

describe('SiswaPage', () => {
  let component: SiswaPage;
  let fixture: ComponentFixture<SiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
