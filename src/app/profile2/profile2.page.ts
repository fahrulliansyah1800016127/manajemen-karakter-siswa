import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-profile2',
  templateUrl: './profile2.page.html',
  styleUrls: ['./profile2.page.scss'],
})
export class Profile2Page implements OnInit {

  
    user: any;
  
    constructor
    (
      private auth: AuthService,
      private router: Router
    ) { }
  
    ngOnInit() 
    {
      this.auth.user$.subscribe(user => {
        this.user = user;
      })
    }
  
    editProfile()
    {
      this.router.navigate(['/profile2/edit2']);
    }
  

}
