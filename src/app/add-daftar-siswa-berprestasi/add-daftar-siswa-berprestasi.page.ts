import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-add-daftar-siswa-berprestasi',
  templateUrl: './add-daftar-siswa-berprestasi.page.html',
  styleUrls: ['./add-daftar-siswa-berprestasi.page.scss'],
})
export class AddDaftarSiswaBerprestasiPage implements OnInit {

  xips ={} as User;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) {}
  
  ngOnInit() {}


  async createKelas(xips: User) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("xips").add(xips);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("daftar-siswa-berprestasi");
    }
  }

  formValidation() {
    if (!this.xips.namasiswa) {
      // show toast message
      this.showToast("Enter Nama Siswa");
      return false;
    }

    
    if (!this.xips.namakelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.xips.prestasi) {
      // show toast message
      this.showToast("Enter Prestasi");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
