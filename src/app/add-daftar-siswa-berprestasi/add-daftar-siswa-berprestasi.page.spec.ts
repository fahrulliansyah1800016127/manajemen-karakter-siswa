import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddDaftarSiswaBerprestasiPage } from './add-daftar-siswa-berprestasi.page';

describe('AddDaftarSiswaBerprestasiPage', () => {
  let component: AddDaftarSiswaBerprestasiPage;
  let fixture: ComponentFixture<AddDaftarSiswaBerprestasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDaftarSiswaBerprestasiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddDaftarSiswaBerprestasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
