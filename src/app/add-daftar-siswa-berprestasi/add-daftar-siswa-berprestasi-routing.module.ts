import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddDaftarSiswaBerprestasiPage } from './add-daftar-siswa-berprestasi.page';

const routes: Routes = [
  {
    path: '',
    component: AddDaftarSiswaBerprestasiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddDaftarSiswaBerprestasiPageRoutingModule {}
