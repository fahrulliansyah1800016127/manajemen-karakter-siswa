import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddDaftarSiswaBerprestasiPageRoutingModule } from './add-daftar-siswa-berprestasi-routing.module';

import { AddDaftarSiswaBerprestasiPage } from './add-daftar-siswa-berprestasi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddDaftarSiswaBerprestasiPageRoutingModule
  ],
  declarations: [AddDaftarSiswaBerprestasiPage]
})
export class AddDaftarSiswaBerprestasiPageModule {}
