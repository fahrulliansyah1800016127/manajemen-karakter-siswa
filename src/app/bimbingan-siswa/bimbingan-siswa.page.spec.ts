import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BimbinganSiswaPage } from './bimbingan-siswa.page';

describe('BimbinganSiswaPage', () => {
  let component: BimbinganSiswaPage;
  let fixture: ComponentFixture<BimbinganSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BimbinganSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BimbinganSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
