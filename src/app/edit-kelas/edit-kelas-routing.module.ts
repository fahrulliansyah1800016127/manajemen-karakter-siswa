import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditKelasPage } from './edit-kelas.page';

const routes: Routes = [
  {
    path: '',
    component: EditKelasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditKelasPageRoutingModule {}
