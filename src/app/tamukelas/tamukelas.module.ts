import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TamukelasPageRoutingModule } from './tamukelas-routing.module';

import { TamukelasPage } from './tamukelas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TamukelasPageRoutingModule
  ],
  declarations: [TamukelasPage]
})
export class TamukelasPageModule {}
