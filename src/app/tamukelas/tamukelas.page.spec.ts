import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TamukelasPage } from './tamukelas.page';

describe('TamukelasPage', () => {
  let component: TamukelasPage;
  let fixture: ComponentFixture<TamukelasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamukelasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TamukelasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
