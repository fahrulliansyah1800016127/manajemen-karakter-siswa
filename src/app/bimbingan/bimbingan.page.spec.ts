import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BimbinganPage } from './bimbingan.page';

describe('BimbinganPage', () => {
  let component: BimbinganPage;
  let fixture: ComponentFixture<BimbinganPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BimbinganPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BimbinganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
