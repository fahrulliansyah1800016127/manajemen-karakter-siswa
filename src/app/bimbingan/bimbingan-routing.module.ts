import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BimbinganPage } from './bimbingan.page';

const routes: Routes = [
  {
    path: '',
    component: BimbinganPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BimbinganPageRoutingModule {}
