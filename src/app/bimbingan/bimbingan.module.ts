import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BimbinganPageRoutingModule } from './bimbingan-routing.module';

import { BimbinganPage } from './bimbingan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BimbinganPageRoutingModule
  ],
  declarations: [BimbinganPage]
})
export class BimbinganPageModule {}
