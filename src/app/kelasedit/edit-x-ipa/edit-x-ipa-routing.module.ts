import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditXIpaPage } from './edit-x-ipa.page';

const routes: Routes = [
  {
    path: '',
    component: EditXIpaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditXIpaPageRoutingModule {}
