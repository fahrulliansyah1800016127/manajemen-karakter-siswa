import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditXIpaPageRoutingModule } from './edit-x-ipa-routing.module';

import { EditXIpaPage } from './edit-x-ipa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditXIpaPageRoutingModule
  ],
  declarations: [EditXIpaPage]
})
export class EditXIpaPageModule {}
