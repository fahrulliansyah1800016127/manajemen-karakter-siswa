import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { User } from '../models/user';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-add-bimbingan',
  templateUrl: './add-bimbingan.page.html',
  styleUrls: ['./add-bimbingan.page.scss'],
})
export class AddBimbinganPage implements OnInit {

  nkelas: string;
  nguru: string;
  kode: string;
  email: string;
  password: string;
 
 
  

  constructor
  (
    private afs: AngularFirestore,
    private afauth: AngularFireAuth,
    private router: Router,
    private loadingCtrl: LoadingController,
    private toastr: ToastController,
  ) { }

  ngOnInit() {
  }

  async register()
  {
    if(this.nguru && this.nkelas && this.kode)
    {
      const loading = await this.loadingCtrl.create({
        message: 'proccessing..',
        spinner: 'crescent',
        showBackdrop: true
      });

      loading.present();

      this.afauth.createUserWithEmailAndPassword(this.email, this.password)
      .then((data)=> {
     
        this.afs.collection('user').doc(data.user.uid).set({
          'userId': data.user.uid,
          'nkelas': this.nkelas,
          'nguru': this.nguru,
          'kode': this.kode,
        
          'createdAt': Date.now()
        })
        .then(()=> {
          loading.dismiss();
          this.toast('Register Berhasil! Silahkan Chek Email Anda', 'success');
          this.router.navigate(['/tabs/kelas-guru']);
        })
        .catch(error => {
          loading.dismiss();
          this.toast(error.message, 'danger');
        })
      })
        .catch(error => {
          loading.dismiss();
          this.toast(error.message, 'danger');
      })
    } else {
      this.toast('Silahkan isi kolom yang kosong!', 'warning');
    }
  } // end of register

  async toast(message, status)
  {
    const toast = await this.toastr.create({
      message: message,
      color: status,
      position: 'top',
      duration: 2000
    });

    toast.present();
  } // end of toast
}


