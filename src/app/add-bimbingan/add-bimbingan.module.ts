import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddBimbinganPageRoutingModule } from './add-bimbingan-routing.module';

import { AddBimbinganPage } from './add-bimbingan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddBimbinganPageRoutingModule
  ],
  declarations: [AddBimbinganPage]
})
export class AddBimbinganPageModule {}
