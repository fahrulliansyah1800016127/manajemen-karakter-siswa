import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-x-ipa',
  templateUrl: './x-ipa.page.html',
  styleUrls: ['./x-ipa.page.scss'],
})
export class XIpaPage {
  xipa: any;
  subscription: any;


  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getXipa() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("xipa")

        .snapshotChanges()
        .subscribe(data => {
          this.xipa = data.map(e => {
            return {
              id: e.payload.doc.id,
              namaguru: e.payload.doc.data()["namaguru"],
              phone: e.payload.doc.data()["phone"],
              namakelas: e.payload.doc.data()["namakelas"],
              namasiswa: e.payload.doc.data()["namasiswa"],
              karakter: e.payload.doc.data()["karakter"],
              penanganan: e.payload.doc.data()["penanganan"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("xipa/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getXipa();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }


}
