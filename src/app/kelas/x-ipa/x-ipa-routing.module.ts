import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { XIpaPage } from './x-ipa.page';

const routes: Routes = [
  {
    path: '',
    component: XIpaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class XIpaPageRoutingModule {}
