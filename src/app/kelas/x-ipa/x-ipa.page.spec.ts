import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { XIpaPage } from './x-ipa.page';

describe('XIpaPage', () => {
  let component: XIpaPage;
  let fixture: ComponentFixture<XIpaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XIpaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(XIpaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
