import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { XIpaPageRoutingModule } from './x-ipa-routing.module';

import { XIpaPage } from './x-ipa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    XIpaPageRoutingModule
  ],
  declarations: [XIpaPage]
})
export class XIpaPageModule {}
