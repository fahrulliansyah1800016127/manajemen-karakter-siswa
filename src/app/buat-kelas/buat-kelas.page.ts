import { Component, OnInit } from '@angular/core';

import '@firebase/auth';
import firebase from 'firebase/app';
import 'firebase/firestore'
import { AlertController } from '@ionic/angular';
import { Router} from '@angular/router';


@Component({
  selector: 'app-buat-kelas',
  templateUrl: './buat-kelas.page.html',
  styleUrls: ['./buat-kelas.page.scss'],
})
export class BuatKelasPage {

  namakelas: string = "";
  namawalikelas: string = "";
  kode: string = "";
  


  constructor(public alert:AlertController, public router:Router) { }

  async simpan(){

    firebase.firestore().collection("datakelas").add({
      namakelas: this.namakelas,
      namawalikelas: this.namawalikelas,
      kode: this.kode,
      date: firebase.firestore.FieldValue.serverTimestamp(), 
    }).then((data) =>
    {
    this.showAlert("Berhasil Menyimpan Data", "Data Berhasil Disimpan");
    console.log("Berhasil Menyimpan Data")
    this.router.navigate(['/tabs/home'])
    }).catch((err)=>{
      this.showAlert("Gagal Menyimpan Data", err.message);
      console.log("Gagal Menyimpan Data")
    })
    
  }

  async showAlert(header:string, message:string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }

}
