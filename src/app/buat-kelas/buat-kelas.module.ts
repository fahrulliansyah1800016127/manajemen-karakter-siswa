import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuatKelasPageRoutingModule } from './buat-kelas-routing.module';

import { BuatKelasPage } from './buat-kelas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuatKelasPageRoutingModule
  ],
  declarations: [BuatKelasPage]
})
export class BuatKelasPageModule {}
