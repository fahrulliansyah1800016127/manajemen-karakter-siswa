import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuatKelasPage } from './buat-kelas.page';

describe('BuatKelasPage', () => {
  let component: BuatKelasPage;
  let fixture: ComponentFixture<BuatKelasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuatKelasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuatKelasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
