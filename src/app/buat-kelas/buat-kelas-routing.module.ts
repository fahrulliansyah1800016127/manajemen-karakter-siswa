import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuatKelasPage } from './buat-kelas.page';

const routes: Routes = [
  {
    path: '',
    component: BuatKelasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuatKelasPageRoutingModule {}
