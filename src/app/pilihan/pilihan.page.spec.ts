import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilihanPage } from './pilihan.page';

describe('PilihanPage', () => {
  let component: PilihanPage;
  let fixture: ComponentFixture<PilihanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilihanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilihanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
