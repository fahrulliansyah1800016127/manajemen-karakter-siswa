import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarSiswaBerprestasiPage } from './daftar-siswa-berprestasi.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarSiswaBerprestasiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarSiswaBerprestasiPageRoutingModule {}
