import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarSiswaBerprestasiPage } from './daftar-siswa-berprestasi.page';

describe('DaftarSiswaBerprestasiPage', () => {
  let component: DaftarSiswaBerprestasiPage;
  let fixture: ComponentFixture<DaftarSiswaBerprestasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarSiswaBerprestasiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarSiswaBerprestasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
