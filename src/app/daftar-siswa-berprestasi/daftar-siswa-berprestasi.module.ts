import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarSiswaBerprestasiPageRoutingModule } from './daftar-siswa-berprestasi-routing.module';

import { DaftarSiswaBerprestasiPage } from './daftar-siswa-berprestasi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarSiswaBerprestasiPageRoutingModule
  ],
  declarations: [DaftarSiswaBerprestasiPage]
})
export class DaftarSiswaBerprestasiPageModule {}
