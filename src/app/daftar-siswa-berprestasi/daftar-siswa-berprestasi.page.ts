import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-daftar-siswa-berprestasi',
  templateUrl: './daftar-siswa-berprestasi.page.html',
  styleUrls: ['./daftar-siswa-berprestasi.page.scss'],
})
export class DaftarSiswaBerprestasiPage  {

  xips: any;
  subscription: any;


  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getXips() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("xips")

        .snapshotChanges()
        .subscribe(data => {
          this.xips = data.map(e => {
            return {
              id: e.payload.doc.id,
              namaguru: e.payload.doc.data()["namaguru"],
              phone: e.payload.doc.data()["phone"],
              namakelas: e.payload.doc.data()["namakelas"],
              namasiswa: e.payload.doc.data()["namasiswa"],
              prestasi: e.payload.doc.data()["prestasi"],

            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("xips/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getXips();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }



}
