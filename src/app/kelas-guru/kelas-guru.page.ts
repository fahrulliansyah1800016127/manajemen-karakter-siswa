import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform, AlertController,  } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-kelas-guru',
  templateUrl: './kelas-guru.page.html',
  styleUrls: ['./kelas-guru.page.scss'],
})
export class KelasGuruPage {
  datakelas: any;
  subscription: any;
 

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    public alertController: AlertController
   
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getDataKelas() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("datakelas")
        .snapshotChanges()
        .subscribe(data => {
          this.datakelas = data.map(e => {
            return {
              id: e.payload.doc.id,
              nkelas: e.payload.doc.data()["nkelas"],
              nguru: e.payload.doc.data()["nguru"],
              kode: e.payload.doc.data()["kode"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.alertController.create({
      message: 'berhasil di hapus',
 
      
     
    });
    loader.present();

    await this.firestore.doc("datakelas/" + id).delete();

    // dismiss loader
    loader.dismiss();
  
      
  

   


    // dismiss loader
   
  }

  ionViewWillEnter() {
    this.getDataKelas();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }


}
