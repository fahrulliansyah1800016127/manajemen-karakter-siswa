import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KelasGuruPage } from './kelas-guru.page';

describe('KelasGuruPage', () => {
  let component: KelasGuruPage;
  let fixture: ComponentFixture<KelasGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KelasGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KelasGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
