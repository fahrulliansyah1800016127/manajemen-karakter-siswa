export interface User 
{
    userId: string;
    userName: string;
    userNip: number;
    userNisn: number;
    userEmail: string;
    userPhone: number;
    userGenre: string;
    userPhoto: string;
    createdAt: number;
    Alamat: string;

 
    namaguru: string;
    namakelas: string;

    namasiswa: string;
    karakter: string;
    penanganan: string;
    phone: string;
    prestasi: string;
}
