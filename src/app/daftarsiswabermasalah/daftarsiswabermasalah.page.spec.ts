import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarsiswabermasalahPage } from './daftarsiswabermasalah.page';

describe('DaftarsiswabermasalahPage', () => {
  let component: DaftarsiswabermasalahPage;
  let fixture: ComponentFixture<DaftarsiswabermasalahPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarsiswabermasalahPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarsiswabermasalahPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
