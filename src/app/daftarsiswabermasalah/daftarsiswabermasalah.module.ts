import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarsiswabermasalahPageRoutingModule } from './daftarsiswabermasalah-routing.module';

import { DaftarsiswabermasalahPage } from './daftarsiswabermasalah.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarsiswabermasalahPageRoutingModule
  ],
  declarations: [DaftarsiswabermasalahPage]
})
export class DaftarsiswabermasalahPageModule {}
