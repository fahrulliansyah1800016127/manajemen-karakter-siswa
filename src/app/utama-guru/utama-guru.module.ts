import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtamaGuruPageRoutingModule } from './utama-guru-routing.module';

import { UtamaGuruPage } from './utama-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UtamaGuruPageRoutingModule
  ],
  declarations: [UtamaGuruPage]
})
export class UtamaGuruPageModule {}
