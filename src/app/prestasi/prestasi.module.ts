import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrestasiPageRoutingModule } from './prestasi-routing.module';

import { PrestasiPage } from './prestasi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrestasiPageRoutingModule
  ],
  declarations: [PrestasiPage]
})
export class PrestasiPageModule {}
