import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrestasiPage } from './prestasi.page';

describe('PrestasiPage', () => {
  let component: PrestasiPage;
  let fixture: ComponentFixture<PrestasiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrestasiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrestasiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
