import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrestasiPage } from './prestasi.page';

const routes: Routes = [
  {
    path: '',
    component: PrestasiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrestasiPageRoutingModule {}
