import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Setting2Page } from './setting2.page';

const routes: Routes = [
  {
    path: '',
    component: Setting2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Setting2PageRoutingModule {}
