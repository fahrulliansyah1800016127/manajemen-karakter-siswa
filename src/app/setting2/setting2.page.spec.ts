import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Setting2Page } from './setting2.page';

describe('Setting2Page', () => {
  let component: Setting2Page;
  let fixture: ComponentFixture<Setting2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Setting2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Setting2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
