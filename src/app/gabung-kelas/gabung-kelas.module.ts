import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GabungKelasPageRoutingModule } from './gabung-kelas-routing.module';

import { GabungKelasPage } from './gabung-kelas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GabungKelasPageRoutingModule
  ],
  declarations: [GabungKelasPage]
})
export class GabungKelasPageModule {}
