import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GabungKelasPage } from './gabung-kelas.page';

const routes: Routes = [
  {
    path: '',
    component: GabungKelasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GabungKelasPageRoutingModule {}
