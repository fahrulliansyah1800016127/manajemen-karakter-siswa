import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GabungKelasPage } from './gabung-kelas.page';

describe('GabungKelasPage', () => {
  let component: GabungKelasPage;
  let fixture: ComponentFixture<GabungKelasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GabungKelasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GabungKelasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
