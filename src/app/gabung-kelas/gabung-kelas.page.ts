import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Kelas } from '../models/kelas.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-gabung-kelas',
  templateUrl: './gabung-kelas.page.html',
  styleUrls: ['./gabung-kelas.page.scss'],
})
export class GabungKelasPage implements OnInit {

  kelas = {} as Kelas;
  kode: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.kode = this.actRoute.snapshot.paramMap.get("kode");
  }
 
 
  ngOnInit() {
    this.getKelasById(this.kode);
  }

  async getKelasById(kode: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    this.firestore
      .doc("kelass/")
      .valueChanges()
      .subscribe(data => {
        this.kelas.nkelas = data["nkelas"];
        this.kelas.nguru = data["nguru"];
        this.kelas.kode = data["kode"];

        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(kelas: Kelas) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("kelass/" + this.kode).update(kelas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("siswa");
    }
  }

  formValidation() {


    if (!this.kelas.kode) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}