import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditDaftarSiswaBerprestasiPage } from './edit-daftar-siswa-berprestasi.page';

const routes: Routes = [
  {
    path: '',
    component: EditDaftarSiswaBerprestasiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditDaftarSiswaBerprestasiPageRoutingModule {}
