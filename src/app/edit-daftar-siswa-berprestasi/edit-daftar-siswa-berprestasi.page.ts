import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-edit-daftar-siswa-berprestasi',
  templateUrl: './edit-daftar-siswa-berprestasi.page.html',
  styleUrls: ['./edit-daftar-siswa-berprestasi.page.scss'],
})
export class EditDaftarSiswaBerprestasiPage implements OnInit {

  xips = {} as User;
  id: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.id = this.actRoute.snapshot.paramMap.get("id");
  }
 
 
  ngOnInit() {
    this.getKelasById(this.id);
  }

  async getKelasById(id: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    this.firestore
      .doc("xips/" + id)
      .valueChanges()
      .subscribe(data => {
        this.xips.namasiswa = data["namasiswa"];
        this.xips.namakelas = data["namakelas"];      
        this.xips.prestasi = data["prestasi"];


        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(xips: User) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("xips/" + this.id).update(xips);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("daftar-siswa-berprestasi");
    }
  }

  formValidation() {
    if (!this.xips.namasiswa) {
      // show toast message
      this.showToast("Enter Nama Siswa");
      return false;
    }

    if (!this.xips.namakelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.xips.prestasi) {
      // show toast message
      this.showToast("Enter Prestasi");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}
